//! This is a simple web-game that can compile to WebAssembly and run in the browser.
//!
//! It expects some pre-defined HTML-elements when it starts up and is intended to be loaded by the
//! index.html file included in this project.
use std::rc::Rc;

use wasm_bindgen::prelude::*;

use crate::game_event_handler::GameEventHandler;
use crate::game_event_queue::{GameEvent, GameEventQueue};
use crate::game_grid::GameGrid;
use crate::game_screen::GameScreen;
use crate::input::Input;
use crate::screen_transition::ScreenTransition;

mod game_grid;
mod game_screen;
mod dom;
mod console;
mod input;
mod game_event_queue;
mod game_event_handler;
mod screen_transition;

/// The main entry point of the app. Gets called when the WebAssembly of this library gets loaded.
#[wasm_bindgen(start)]
pub fn main() {
    // Set up everything for the game. Inject all dependencies through the constructor.
    let event_queue = Rc::new(GameEventQueue::new());
    let game_screen = GameScreen::new(GameGrid::new(), Rc::clone(&event_queue));
    let screen_transition = ScreenTransition::new();
    let mut event_handler = GameEventHandler::new(
        Rc::clone(&event_queue),
        game_screen,
        screen_transition,
    );

    // Start the game by emitting an event and letting the handler handle it.
    event_queue.emit(GameEvent::Welcome);
    event_handler.handle_queued();

    // Set up input bindings that handle input and generate more events.
    let input = Input::new(
        Rc::clone(&event_queue),
        event_handler
    );
    input.register_input_events();
}