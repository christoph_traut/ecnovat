use std::rc::Rc;

use wasm_bindgen::prelude::*;
use web_sys;
use web_sys::HtmlElement;

use crate::dom;
use crate::game_event_queue::{GameEvent, GameEventQueue};
use crate::game_grid::GameGrid;

/// Responsible for updating both the internal representation of the grid and the HTML that is shown to
/// the user.
pub struct GameScreen {
    game_grid: GameGrid,
    event_queue: Rc<GameEventQueue>
}

impl GameScreen {
    pub fn new(game_grid: GameGrid, event_queue: Rc<GameEventQueue>) -> GameScreen {
        GameScreen { game_grid, event_queue }
    }

    pub fn move_up(&mut self) {
        self.game_grid.move_up();
        self.update_after_move();
    }

    pub fn move_right(&mut self) {
        self.game_grid.move_right();
        self.update_after_move();
    }

    pub fn move_down(&mut self) {
        self.game_grid.move_down();
        self.update_after_move();
    }

    pub fn move_left(&mut self) {
        self.game_grid.move_left();
        self.update_after_move();
    }

    pub fn set_up_game_screen(&mut self, solution: &str) {
        self.clean_up_html();
        self.set_up_html(solution).expect("Failed to set up HTML for the game screen.");
        self.game_grid.reset_rows(solution);
        self.game_grid.shuffle();
        self.update_html_grid();
    }

    fn clean_up_html(&self) {
        for (index, _) in self.game_grid.ids_with_position() {
            let element: HtmlElement = dom::get_element_by_id(&format!("grid-item-{}", index));
            element.remove();
        }
    }

    fn set_up_html(&self, solution: &str) -> Result<(), JsValue> {
        let chars: Vec<char> = solution.chars().collect();
        let size = chars.len();
        let game_grid: HtmlElement = dom::get_element_by_id("game-grid");
        game_grid.style().set_property("--grid-size", &size.to_string())?;
        for row in 0..size {
            for column in 0..size {
                let index = row * size + column;
                let c = chars[column];
                let val: HtmlElement = dom::create_element("div");
                val.set_id(&format!("grid-item-{}", index));
                val.set_inner_html(&c.to_string());
                game_grid.append_child(&val)?;
            }
        }

        let solution_preview = (String::from(solution) + "\n").repeat(size);
        dom::get_element_by_id::<HtmlElement>("solution-preview")
            .set_inner_html(&solution_preview);

        Ok(())
    }

    fn update_after_move(&self) {
        self.update_html_grid();
        if self.game_grid.is_solved() {
            self.event_queue.emit(GameEvent::PlayerWins);
        }
    }

    fn update_html_grid(&self) {
        for (id, (row, column)) in self.game_grid.ids_with_position() {
            let cell: HtmlElement = dom::get_element_by_id(&format!("grid-item-{}", id));
            cell.style()
                .set_property("grid-area", &format!("{} / {}", row + 2, column + 2))
                .expect(&format!("Failed to update style for element grid-item-{}.", id));
        }
        let (active_row, active_column) = self.game_grid.active_cell();
        // Row/Column highlight
        dom::get_element_by_id::<HtmlElement>("grid-selected-row")
            .style()
            .set_property("grid-area", &format!("{} / 2 / {} / calc(var(--grid-size) + 2)", active_row + 2, active_row + 2))
            .expect("Failed to update style for element grid-selected-row");
        dom::get_element_by_id::<HtmlElement>("grid-selected-column")
            .style()
            .set_property("grid-area", &format!("2 / {} / calc(var(--grid-size) + 2) / {}", active_column + 2, active_column + 2))
            .expect("Failed to update style for element grid-selected-column");
        // Buttons
        dom::get_element_by_id::<HtmlElement>("move-up")
            .style()
            .set_property("grid-area", &format!("1 / {}", active_column + 2))
            .expect("Failed to update style for element move-up");
        dom::get_element_by_id::<HtmlElement>("move-right")
            .style()
            .set_property("grid-area", &format!("{} / calc(var(--grid-size) + 2)", active_row + 2))
            .expect("Failed to update style for element move-right");
        dom::get_element_by_id::<HtmlElement>("move-down")
            .style()
            .set_property("grid-area", &format!("calc(var(--grid-size) + 2) / {}", active_column + 2))
            .expect("Failed to update style for element move_down");
        dom::get_element_by_id::<HtmlElement>("move-left")
            .style()
            .set_property("grid-area", &format!("{} / 1", active_row + 2))
            .expect("Failed to update style for element move-left");
    }
}