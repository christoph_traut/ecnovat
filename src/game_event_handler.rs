use std::rc::Rc;

use crate::screen_transition::{ScreenTransition};
use crate::game_event_queue::{GameEventQueue, GameEvent};
use crate::game_screen::GameScreen;
use crate::console_log;

/// Implements the main game logic by reacting to game events and telling the different components
/// of the game what to do.
pub struct GameEventHandler {
    events_queue: Rc<GameEventQueue>,
    game_screen: GameScreen,
    screen: ScreenTransition,
}

impl GameEventHandler {
    pub fn new(
        events_queue: Rc<GameEventQueue>,
        game_screen: GameScreen,
        screen_transition: ScreenTransition,
    ) -> GameEventHandler
    {
        GameEventHandler {
            events_queue,
            game_screen,
            screen: screen_transition,
        }
    }

    pub fn handle_queued(&mut self) {
        while let Some(event) = self.events_queue.handle() {
            match event {
                GameEvent::MoveUp if self.screen.is_game() => self.game_screen.move_up(),
                GameEvent::MoveRight if self.screen.is_game() => self.game_screen.move_right(),
                GameEvent::MoveDown if self.screen.is_game() => self.game_screen.move_down(),
                GameEvent::MoveLeft if self.screen.is_game() => self.game_screen.move_left(),
                GameEvent::PlayerWins => {
                    self.screen.to_win();
                },
                GameEvent::StartGame(solution) => {
                    self.game_screen.set_up_game_screen(&solution);
                    self.screen.to_game();
                }
                GameEvent::Welcome => {
                    self.screen.to_welcome();
                }
                e => console_log!("Ignored event: {:?}", e),
            }
        }
    }
}