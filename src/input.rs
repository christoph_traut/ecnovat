use std::cell::RefCell;
use std::rc::Rc;

use crate::dom;
use crate::game_event_handler::GameEventHandler;
use crate::game_event_queue::{GameEvent, GameEventQueue};

/// Syntactic sugar for binding clicks on HTML-buttons to game events.
macro_rules! bind_buttons {
    ($input:expr, $($button_id:literal => $event:expr),*) => {$(
        let event_queue_ref = Rc::clone(&$input.event_queue);
        let event_handler_ref = Rc::clone(&$input.event_handler);
        dom::onclick_button($button_id, move || {
            event_queue_ref.emit($event);
            event_handler_ref.borrow_mut().handle_queued();
        });
    )*};
}

/// Syntactic sugar for binding keyboard key presses to game events.
macro_rules! bind_keys {
    ($input:expr, $($keycode:literal => $event:expr),*) => {
        let event_queue_ref = Rc::clone(&$input.event_queue);
        let event_handler_ref = Rc::clone(&$input.event_handler);
        dom::onkeydown(move |event| {
            match event.key_code() {
                $($keycode => {
                    event_queue_ref.emit($event);
                    event_handler_ref.borrow_mut().handle_queued();
                })*
                _ => (),
            };
        });
    }
}

/// The input handler.
/// Is responsible for reacting to input events and translating them to game events.
/// Also calls the game event handler afterwards to make sure that all events get handled.
pub struct Input {
    event_queue: Rc<GameEventQueue>,
    event_handler: Rc<RefCell<GameEventHandler>>,
}

impl Input {
    pub fn new(event_queue: Rc<GameEventQueue>, event_handler: GameEventHandler) -> Input {
        Input {
            event_queue,
            event_handler: Rc::new(RefCell::new(event_handler)),
        }
    }

    pub fn register_input_events(&self) {
        bind_buttons!(self,
          "restart-game" => GameEvent::Welcome,
          "back-to-welcome" => GameEvent::Welcome,
          "game-bee" => GameEvent::StartGame(String::from("BEE")),
          "game-fun" => GameEvent::StartGame(String::from("FUN")),
          "game-emma" => GameEvent::StartGame(String::from("EMMA")),
          "game-novatec" => GameEvent::StartGame(String::from("NOVATEC")),
          "move-up" => GameEvent::MoveUp,
          "move-right" => GameEvent::MoveRight,
          "move-down" => GameEvent::MoveDown,
          "move-left" => GameEvent::MoveLeft
        );

        bind_keys!(self,
            37 => GameEvent::MoveLeft,
            38 => GameEvent::MoveUp,
            39 => GameEvent::MoveRight,
            40 => GameEvent::MoveDown
        );
    }
}
