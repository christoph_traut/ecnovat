use wasm_bindgen::prelude::*;

/// Import binding for console.log() in the browser.
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);
}

/// Simple macro that takes a variable number of arguments and passes them to console.log.
#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => (crate::console::log(&format_args!($($t)*).to_string()))
}
