use crate::dom;
use web_sys::HtmlElement;

enum Screen {
    Welcome,
    Game,
    YouWin,
}

/// Responsible for switching between the different screens of the game and keeping track of the
/// screen, that we are currently on.
pub struct ScreenTransition {
    current_screen: Option<Screen>
}

impl ScreenTransition {
    pub fn new() -> ScreenTransition {
        ScreenTransition {
            current_screen: None
        }
    }

    pub fn is_game(&self) -> bool {
        match self.current_screen {
            Some(Screen::Game) => true,
            _ => false
        }
    }

    pub fn to_welcome(&mut self) {
        dom::get_element_by_id::<HtmlElement>("welcome-page")
            .style()
            .set_property("display", "flex")
            .expect("Couldn't show #welcome-page!");
        dom::get_element_by_id::<HtmlElement>("game-page")
            .style()
            .set_property("display", "none")
            .expect("Couldn't show #welcome-page!");
        dom::get_element_by_id::<HtmlElement>("you-won-screen")
            .style()
            .set_property("display", "none")
            .expect("Couldn't hide #you-won-screen!");

        self.current_screen = Some(Screen::Welcome);
    }

    pub fn to_game(&mut self) {
        dom::get_element_by_id::<HtmlElement>("welcome-page")
            .style()
            .set_property("display", "none")
            .expect("Couldn't show #welcome-page!");
        dom::get_element_by_id::<HtmlElement>("game-page")
            .style()
            .set_property("display", "flex")
            .expect("Couldn't show #welcome-page!");

        self.current_screen = Some(Screen::Game);
    }

    pub fn to_win(&mut self) {
        dom::get_element_by_id::<HtmlElement>("you-won-screen")
            .style()
            .set_property("display", "flex")
            .expect("Couldn't show #you-won-screen!");

        self.current_screen = Some(Screen::YouWin);
    }
}
