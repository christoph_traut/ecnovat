use rand::{thread_rng, Rng};

/// A single cell on the game-grid, i.e. one of the letters that need to be arranged.
#[derive(Clone, Copy, PartialEq)]
struct Cell {
    /// Uniquely identifies it.
    id: usize,
    /// The value that is displayed.
    value: char,
}

/// The square grid of letters that needs to be rearranged.
pub struct GameGrid {
    /// How many rows/columns the square has.
    size: usize,
    /// The list of cells. Rows are stored consecutively in memory.
    cells: Vec<Cell>,
    /// The cell that the cursor is at. Stored as (row-index, column-index).
    active_cell: (usize, usize),
    /// What a solved row should look like. Uses for checking whether the grid is in a solved state.
    solved_row: Vec<char>,
}

impl GameGrid {
    pub fn new() -> GameGrid {
        GameGrid::with_rows(&vec![])
    }

    fn with_rows(solved_row: &[char]) -> GameGrid {
        let size = solved_row.len();
        let cells = (0..size * size).map(|id| Cell { id, value: solved_row[id % size] }).collect();
        let active_cell = (0, 0);
        let solved_row = Vec::from(solved_row);
        GameGrid {
            size, cells, active_cell, solved_row,
        }
    }

    pub fn reset_rows(&mut self, solution: &str) {
        let values: Vec<char> = solution.chars().collect();
        *self = GameGrid::with_rows(&values);
    }

    pub fn move_up(&mut self) {
        let (row, column) = self.active_cell;
        self.active_cell = ((row + self.size - 1) % self.size, column);
        self.move_column_up(column);
    }

    pub fn move_right(&mut self) {
        let (row, column) = self.active_cell;
        self.active_cell = (row, (column + 1) % self.size);
        self.move_row_right(row);
    }

    pub fn move_down(&mut self) {
        let (row, column) = self.active_cell;
        self.active_cell = ((row + 1) % self.size, column);
        self.move_column_down(column);
    }

    pub fn move_left(&mut self) {
        let (row, column) = self.active_cell;
        self.active_cell = (row, (column + self.size - 1) % self.size);
        self.move_row_left(row);
    }

    pub fn ids_with_position(&self) -> Vec<(usize, (usize, usize))> {
        self.cells.iter()
            .enumerate()
            .map(|(index, c)| (c.id, self.coordinates_of(index)))
            .collect()
    }

    pub fn is_solved(&self) -> bool {
        for row in 0..self.size {
            let row_start = self.index_of(row, 0);
            let row_end = self.index_of(row, self.size-1);
            let row = self.cells[row_start..=row_end].iter().map(|c| c.value).collect::<Vec<_>>();
            if row != self.solved_row {
                return false
            }
        }
        true
    }

    pub fn shuffle(&mut self) {
        let mut rng = thread_rng();
        while self.is_solved() {
            for _ in 0..self.size*10 {
                let direction = rng.gen_range(0..4);
                let steps = rng.gen_range(1..=self.size);
                for _ in 0..steps {
                    match direction {
                        0 => self.move_up(),
                        1 => self.move_right(),
                        2 => self.move_down(),
                        3 => self.move_left(),
                        _ => panic!("Got random value out of range!")
                    }
                }
            }
        }
    }

    pub fn active_cell(&self) -> (usize, usize) {
        self.active_cell
    }

    fn move_row_right(&mut self, row: usize) {
        let last_value = self.value_at(row, self.size - 1);
        for column in (1..self.size).rev() {
            self.set_value_at(row, column, self.value_at(row, column - 1));
        }
        self.set_value_at(row, 0, last_value);
    }

    fn move_row_left(&mut self, row: usize) {
        let first_value = self.value_at(row, 0);
        for column in 0..self.size-1 {
            self.set_value_at(row, column, self.value_at(row, column + 1));
        }
        self.set_value_at(row, self.size-1, first_value);
    }

    fn move_column_down(&mut self, column: usize) {
        let last_value = self.value_at(self.size - 1, column);
        for row in (1..self.size).rev() {
            self.set_value_at(row, column, self.value_at(row - 1, column));
        }
        self.set_value_at(0, column, last_value);
    }

    fn move_column_up(&mut self, column: usize) {
        let first_value = self.value_at(0, column);
        for row in 0..self.size-1 {
            self.set_value_at(row, column, self.value_at(row + 1, column));
        }
        self.set_value_at(self.size-1, column, first_value);
    }

    fn index_of(&self, row: usize, column: usize) -> usize {
        (column + row * self.size) as usize
    }

    fn coordinates_of(&self, index: usize) -> (usize, usize) {
        (index / self.size, index % self.size)
    }

    fn value_at(&self, row: usize, column: usize) -> Cell {
        self.cells[self.index_of(row, column)]
    }

    fn set_value_at(&mut self, row: usize, column: usize, value: Cell) {
        let index = self.index_of(row, column);
        self.cells[index] = value;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn a_new_3x3_grid_is_filled_correctly() {
        let grid = GameGrid::with_rows(&['A', 'B', 'C']);

        assert_eq!(grid.cells.iter().map(|c| (c.id, c.value)).collect::<Vec<_>>(), vec![
            (00, 'A'), (01, 'B'), (02, 'C'),
            (03, 'A'), (04, 'B'), (05, 'C'),
            (06, 'A'), (07, 'B'), (08, 'C'),
        ]);
    }

    #[test]
    fn moving_up_updates_a_3x3_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_up();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            3, 1, 2,
            6, 4, 5,
            0, 7, 8,
        ]);
    }

    #[test]
    fn moving_right_updates_a_3x3_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_right();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            2, 0, 1,
            3, 4, 5,
            6, 7, 8,
        ]);
    }

    #[test]
    fn moving_down_updates_a_3x3_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_down();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            6, 1, 2,
            0, 4, 5,
            3, 7, 8,
        ]);
    }

    #[test]
    fn moving_left_updates_a_3x3_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_left();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            1, 2, 0,
            3, 4, 5,
            6, 7, 8,
        ]);
    }

    #[test]
    fn a_new_5x5_grid_is_filled_correctly() {
        let grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);

        assert_eq!(grid.cells.iter().map(|c| (c.id, c.value)).collect::<Vec<_>>(), vec![
            (00, 'a'), (01, 'b'), (02, 'c'), (03, 'd'), (04, 'e'),
            (05, 'a'), (06, 'b'), (07, 'c'), (08, 'd'), (09, 'e'),
            (10, 'a'), (11, 'b'), (12, 'c'), (13, 'd'), (14, 'e'),
            (15, 'a'), (16, 'b'), (17, 'c'), (18, 'd'), (19, 'e'),
            (20, 'a'), (21, 'b'), (22, 'c'), (23, 'd'), (24, 'e'),
        ]);
    }

    #[test]
    fn moving_up_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_up();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            05, 01, 02, 03, 04,
            10, 06, 07, 08, 09,
            15, 11, 12, 13, 14,
            20, 16, 17, 18, 19,
            00, 21, 22, 23, 24,
        ]);
    }

    #[test]
    fn moving_right_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_right();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            04, 00, 01, 02, 03,
            05, 06, 07, 08, 09,
            10, 11, 12, 13, 14,
            15, 16, 17, 18, 19,
            20, 21, 22, 23, 24,
        ]);
    }

    #[test]
    fn moving_down_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_down();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            20, 01, 02, 03, 04,
            00, 06, 07, 08, 09,
            05, 11, 12, 13, 14,
            10, 16, 17, 18, 19,
            15, 21, 22, 23, 24,
        ]);
    }

    #[test]
    fn moving_left_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_left();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            01, 02, 03, 04, 00,
            05, 06, 07, 08, 09,
            10, 11, 12, 13, 14,
            15, 16, 17, 18, 19,
            20, 21, 22, 23, 24,
        ]);
    }

    #[test]
    fn moving_right_and_then_down_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_right();
        grid.move_down();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            04, 21, 01, 02, 03,
            05, 00, 07, 08, 09,
            10, 06, 12, 13, 14,
            15, 11, 17, 18, 19,
            20, 16, 22, 23, 24,
        ]);
    }

    #[test]
    fn moving_right_and_then_up_updates_a_5x5_grid_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_right();
        grid.move_up();

        assert_eq!(grid.ids_with_position().iter().map(|(id, _)| *id).collect::<Vec<_>>(), vec![
            04, 06, 01, 02, 03,
            05, 11, 07, 08, 09,
            10, 16, 12, 13, 14,
            15, 21, 17, 18, 19,
            20, 00, 22, 23, 24,
        ]);
    }

    #[test]
    fn the_active_cell_starts_out_as_0() {
        let grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        assert_eq!(grid.active_cell, (0, 0));
    }

    #[test]
    fn moving_up_updates_the_active_cell_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_up();

        assert_eq!(grid.active_cell, (4, 0));
    }

    #[test]
    fn moving_right_updates_the_active_cell_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_right();

        assert_eq!(grid.active_cell, (0, 1));
    }

    #[test]
    fn moving_down_updates_the_active_cell_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_down();

        assert_eq!(grid.active_cell, (1, 0));
    }

    #[test]
    fn moving_left_updates_the_active_cell_correctly() {
        let mut grid = GameGrid::with_rows(&['a', 'b', 'c', 'd', 'e']);
        grid.move_left();

        assert_eq!(grid.active_cell, (0, 4));
    }

    #[test]
    fn a_fresh_3x3_grid_is_solved() {
        let grid = GameGrid::with_rows(&['A', 'B', 'C']);

        assert!(grid.is_solved());
    }

    #[test]
    fn a_3x3_grid_is_no_longer_solved_if_it_is_moved_to_the_right() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_right();

        assert!(!grid.is_solved());
    }

    #[test]
    fn a_3x3_grid_is_no_longer_solved_if_it_is_moved_to_the_left() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_left();

        assert!(!grid.is_solved());
    }

    #[test]
    fn a_3x3_grid_is_still_solved_if_it_is_moved_down() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_down();

        assert!(grid.is_solved());
    }

    #[test]
    fn a_3x3_grid_is_still_solved_if_it_is_moved_up() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_up();

        assert!(grid.is_solved());
    }

    #[test]
    fn moving_right_and_then_back_makes_a_3x3_grid_solved() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_right();
        grid.move_left();

        assert!(grid.is_solved());
    }

    #[test]
    fn moving_right_3_times_keeps_a_3x3_grid_solved() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C']);
        grid.move_right();
        grid.move_right();
        grid.move_right();

        assert!(grid.is_solved());
    }

    #[test]
    fn a_5x5_grid_with_all_equal_characters_is_solved() {
        let grid = GameGrid::with_rows(&['X', 'X', 'X', 'X', 'X']);

        assert!(grid.is_solved());
    }

    #[test]
    fn a_5x5_grid_with_all_equal_characters_cannot_be_unsolved() {
        let mut grid = GameGrid::with_rows(&['X', 'X', 'X', 'X', 'X']);
        grid.move_right();
        grid.move_up();
        grid.move_right();

        assert!(grid.is_solved());
    }

    #[test]
    fn a_shuffled_5x5_grid_is_unsolved() {
        let mut grid = GameGrid::with_rows(&['A', 'B', 'C', 'D', 'E']);
        grid.shuffle();

        assert!(!grid.is_solved());
    }

    #[test]
    fn coordinates_of_is_the_inverse_of_index_of() {
        let grid = GameGrid::with_rows(&['A', 'B', 'C', 'D', 'E']);

        assert_eq!((0, 0), grid.coordinates_of(grid.index_of(0, 0)));
        assert_eq!((0, 1), grid.coordinates_of(grid.index_of(0, 1)));
        assert_eq!((1, 0), grid.coordinates_of(grid.index_of(1, 0)));
        assert_eq!((4, 3), grid.coordinates_of(grid.index_of(4, 3)));
    }
}
