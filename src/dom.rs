//! Static helper methods for working with the DOM.

use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::*;
use web_sys::{Document, HtmlButtonElement, KeyboardEvent};

fn get_document() -> Document {
    web_sys::window()
        .expect("Could not access window!")
        .document()
        .expect("Could not access window.document!")
}

pub fn get_element_by_id<T: JsCast>(id: &str) -> T {
    get_document()
        .get_element_by_id(id)
        .expect(&format!("Failed to find #{}.", id))
        .dyn_into::<T>()
        .expect(&format!("#{} was not the expected type.", id))
}

pub fn create_element<T: JsCast>(tag: &str) -> T {
    get_document()
        .create_element("div")
        .expect(&format!("Could not create element of type {}.", tag))
        .dyn_into::<T>()
        .expect(&format!("Create element of type {} could not be cast into the expected type.", tag))
}

pub fn onclick_button(id: &str, f: impl Fn() + 'static) {
    let button = get_document()
        .get_element_by_id(id)
        .expect(&format!("Failed to find #{}.", id))
        .dyn_into::<HtmlButtonElement>()
        .expect(&format!("#{} was not a button", id));
    let closure = Closure::wrap(Box::new(f) as Box<dyn Fn()>);
    button.set_onclick(Some(closure.as_ref().unchecked_ref()));
    closure.forget();
}

pub fn onkeydown(f: impl Fn(KeyboardEvent) + 'static) {
    let closure = Closure::wrap(Box::new(f) as Box<dyn Fn(KeyboardEvent)>);
    get_document().set_onkeydown(Some(closure.as_ref().unchecked_ref()));
    closure.forget();
}
