use std::cell::RefCell;

/// A game event is something that is emitted to the event queue and should prompt the state of the
/// game to change.
#[derive(Debug, PartialEq)]
pub enum GameEvent {
    /// Move to the welcome screen.
    Welcome,
    /// Move to the game screen with a specific solution string.
    StartGame(String),
    /// The player wants to move the active column up.
    MoveUp,
    /// The player wants to move the active row right.
    MoveRight,
    /// The player wants to move the active column down.
    MoveDown,
    /// The player wants to move the active row left.
    MoveLeft,
    /// The player just solved the grid and won.
    PlayerWins,
}

/// Event queue used for decoupling different parts of the game from each other.
///
/// For example: Instead of knowing how to switch to the "You-Win"-Screen, the game screen just
/// needs to emit a `PlayerWins` message and the event handler will make sure that the message is
/// handled properly.
pub struct GameEventQueue {
    queue: RefCell<Vec<GameEvent>>,
}

impl GameEventQueue {
    pub fn new() -> GameEventQueue {
        GameEventQueue {
            queue: RefCell::new(vec![]),
        }
    }

    pub fn emit(&self, event: GameEvent) {
        self.queue.borrow_mut().push(event);
    }

    pub fn handle(&self) -> Option<GameEvent> {
        if !self.queue.borrow().is_empty() {
            Some(self.queue.borrow_mut().remove(0))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_queue_returns_nothing_to_handle() {
        let events = GameEventQueue::new();

        let event = events.handle();

        assert_eq!(None, event);
    }

    #[test]
    fn single_element_queue_returns_one_event_and_then_nothing() {
        let events = GameEventQueue::new();
        events.emit(GameEvent::PlayerWins);

        let event1 = events.handle();
        let event2 = events.handle();

        assert_eq!(Some(GameEvent::PlayerWins), event1);
        assert_eq!(None, event2);
    }

    #[test]
    fn queue_returns_the_events_in_the_order_they_where_emitted() {
        let events = GameEventQueue::new();
        events.emit(GameEvent::MoveDown);
        events.emit(GameEvent::MoveDown);
        events.emit(GameEvent::PlayerWins);
        events.emit(GameEvent::Welcome);

        let event1 = events.handle();
        let event2 = events.handle();
        let event3 = events.handle();
        let event4 = events.handle();
        let event5 = events.handle();

        assert_eq!(Some(GameEvent::MoveDown), event1);
        assert_eq!(Some(GameEvent::MoveDown), event2);
        assert_eq!(Some(GameEvent::PlayerWins), event3);
        assert_eq!(Some(GameEvent::Welcome), event4);
        assert_eq!(None, event5);
    }
}